package ru.t1.dkozyaikin.tm.api.service;

import ru.t1.dkozyaikin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
